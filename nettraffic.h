#ifndef NETTRAFFIC_H
#define NETTRAFFIC_H

#include <QWidget>
#include <QTableWidget>
#include <QDateTime>
#include <QThread>
#include <QDebug>
#include <QMessageBox>
#include "pcap.h"
#include "netinet/ether.h"
#include "qlistwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class NetTraffic; }
QT_END_NAMESPACE

class NetTraffic : public QWidget
{
	Q_OBJECT
	
public:
	NetTraffic(QWidget *parent = nullptr);
	~NetTraffic();
	
private slots:
	void on_stopButton_clicked();
	void on_clearButton_clicked();
	void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
	
private:
	Ui::NetTraffic *ui;
	typedef struct TcpPacket {
		u_short srcPort;
		u_short dstPort;
		u_int seqNum;
		u_int ackNum;
	} TcpPacket;
	TcpPacket *pTcpPacket = nullptr;
	typedef struct IpPacket {
		char headerLen : 4;
		u_short totalLen;
		u_char protocol;
		struct in_addr srcIp;
		struct in_addr dstIp;
		struct ip_mreq reqIp;
		struct ip_mreq_source reqSrcIp;
	} IpPacket;
	IpPacket *pIpPacket = nullptr;
#define IP_HEADER_LEN(pIpPacket) \
	(((u_short)((((IpPacket *)pIpPacket)->headerLen)) * 4))
#define TCP_PACKET(pIpPacket) \
	((TcpPacket *)(((u_char *)pIpPacket) + IP_HEADER_LEN(pIpPacket)))
	void createTable();
	void findDevices();
	void pcapTraffic();
	void showTable();
	void itemColor();
	void resizeEvent(QResizeEvent *event);
	struct ether_header *pEthHeader = nullptr;
	struct ether_arp *pEthArp = nullptr;
	struct pcap_pkthdr pktHdr;
	QString itemStr = QString();
	QString filter = QString();
	QStringList listDevice = QStringList();
	QTableWidget *table = nullptr;
	QThread *thread = nullptr;
	QTableWidgetItem *item = nullptr;
	pcap_t *pcap = nullptr;
	int rowCount = 0;
	int startClickNumber = 0;
	int filterCount = 0;
	bool stop = false;
};
#endif // NETTRAFFIC_H
