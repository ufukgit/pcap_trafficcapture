#include "nettraffic.h"
#include "ui_nettraffic.h"
#include "iostream"

NetTraffic::NetTraffic(QWidget *parent)
	: QWidget(parent), ui(new Ui::NetTraffic) {
	ui->setupUi(this);
	createTable();
	findDevices();
}

void NetTraffic::createTable()
{
	table = ui->tableWidget;
	table->setColumnCount(6);
	table->setAlternatingRowColors(true);
	table->setSelectionMode(QAbstractItemView::SingleSelection);
	table->setSelectionBehavior(QAbstractItemView::SelectRows);
	table->setStyleSheet("QTableWidget{"
						 "background-color: #FFFFFF;"
						 "selection-background-color: rgb(52, 101, 164);"
						 "}");
	QStringList upLabels;
	upLabels << "Time"
			 << "Source"
			 << "Destination"
			 << "Network Protocol"
			 << "Packet Length"
			 << "Info";
	table->setHorizontalHeaderLabels(upLabels);
}

void NetTraffic::findDevices()
{
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *alldevs = nullptr;
	pcap_if_t *devices = nullptr;
	if (pcap_findalldevs(&alldevs, errbuf) != 0)
	{
		QMessageBox::information(this, tr("ERROR"), tr(errbuf));
		exit(EXIT_FAILURE);
	}
	for(devices = alldevs; devices != NULL; devices = devices->next)
	{
		listDevice.append(devices->name);
	}
	if(!listDevice.size())
		return;
	for (int i = 0; i < listDevice.size(); i++) {
		ui->listWidget->addItem(listDevice.at(i));
	}
	pcap_freealldevs(alldevs);
}

/*
moc = meta object compilerdır. 
qt'un c++ uzantılarını moc işler.
Q_OBJECT macrosu görülürse moc dosyası oluşturulur.(headerda tanımlı)
signal slot yapıalcaksa en üstte Q_OBJECT makrosu tanımlanmalı.
Signal slot yapılırken aslında bu Object ile yapılır.
QObject::connect(....);
Thread incele..
*/

void NetTraffic::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
	rowCount = 0;
	table->clear();
	createTable();
	itemStr = item->text();
	if(itemStr.isEmpty())
		return;
	ui->lineEdit->setText(QString("%1 capturing").arg(itemStr));
	QByteArray itemByte = itemStr.toLocal8Bit();
	if(itemByte.isEmpty())
		return;
	char *dev = itemByte.data();
	char errbuf[PCAP_ERRBUF_SIZE];
	if (dev == NULL) {
		QMessageBox::information(this, tr("ERROR"), tr("Device Not Found"));
		exit(EXIT_FAILURE);
	}
	pcap = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (!pcap) {
		QMessageBox::information(this, tr("ERROR"), tr("Device Cannot Capture Packets"));
		exit(EXIT_FAILURE);
	}
	stop = false;
	thread = QThread::create([this]
	{
		pcapTraffic();
	});
	thread->start();
	return;
}

void NetTraffic::pcapTraffic()
{
	while (1) {
		table->setRowCount(rowCount + 1);
		const u_char *pktAddr = pcap_next(pcap, &pktHdr);
		pIpPacket = (IpPacket *)(pktAddr + ETHER_HDR_LEN);
		pTcpPacket = (TcpPacket *)TCP_PACKET(pIpPacket);
		pEthHeader = (struct ether_header *)pktAddr;
		pEthArp = (struct ether_arp *)pTcpPacket;
		showTable();
		if(stop == true)
			return;
	}
	pcap_close(pcap);
}

void NetTraffic::showTable()
{
	{
		item = new QTableWidgetItem;
		itemColor();
		qint64 timeS = pktHdr.ts.tv_sec;
		QDateTime timestamp;
		timestamp.setTime_t(timeS);
		item->setText(timestamp.toString(Qt::SystemLocaleShortDate));
		table->setItem(rowCount, 0, item);
	}
	{
		item = new QTableWidgetItem;
		if (pEthHeader->ether_type == 8) {
			itemColor();
			item->setText(QString::fromLocal8Bit(inet_ntoa(pIpPacket->dstIp)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_PUP) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_SPRITE) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IP) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_ARP) {
			itemColor();
			QString device = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost));
			if (device.contains("0:61:b2")) {
				device = device.replace("e0:61:b2:", "Hangzhou_");
				item->setText(device);
			}
			else if (device.contains("94:e1:ac")) {
				device = device.replace("94:e1:ac:", "Hangzhou_");
				item->setText(device);
			}
			else if (device.contains("0:10:f3")) {
				device = device.replace("0:10:f3:", "NexcomIn_");
				item->setText(device);
			}
			else if (device.contains("0:e0:4c")) {
				device = device.replace("0:e0:4c:", "RealtekS_");
				item->setText(device);
			}
			else if (device.contains("94:18:82")) {
				device = device.replace("94:18:82:", "HewlettP_");
				item->setText(device);
			}
			else if (device.contains("7c:a2:3e")) {
				device = device.replace("7c:a2:3e:", "HuaweiTe_");
				item->setText(device);
			}
			else if (device.contains("40:8d:5c")) {
				device = device.replace("40:8d:5c:", "GigaByte_");
				item->setText(device);
			}
			else
				item->setText(device);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_REVARP) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_AT) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_AARP) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_VLAN) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IPX) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IPV6) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_LOOPBACK) {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		else {
			itemColor();
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_shost)));
		}
		table->setItem(rowCount, 1, item);
	}
	{ 
		item = new QTableWidgetItem;
		if (pEthHeader->ether_type == 8) {
			itemColor();
			item->setText(QString::fromLocal8Bit(inet_ntoa(pIpPacket->reqIp.imr_multiaddr)));
		}
		else if (pEthHeader->ether_type == ETHERTYPE_PUP) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_SPRITE) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IP) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_ARP) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_REVARP) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_AT) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_AARP) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_VLAN) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IPX) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_IPV6) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else if (pEthHeader->ether_type == ETHERTYPE_LOOPBACK) {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		else {
			itemColor();
			QString strDest = QString::fromLocal8Bit(ether_ntoa((const ether_addr *)pEthHeader->ether_dhost));
			if (strDest == "ff:ff:ff:ff:ff:ff") 
				item->setText("Broadcast");
			else
				item->setText(strDest);
		}
		table->setItem(rowCount, 2, item);
	}
	{
		item = new QTableWidgetItem;
		if (pEthHeader->ether_type == 8) {
			itemColor();
			item->setText("IPv4");
		}
		else if (pEthHeader->ether_type == 512) {
			itemColor();
			item->setText("PUP");
		}
		else if (pEthHeader->ether_type == 1280) {
			itemColor();
			item->setText("SPRITE");
		}
		else if (pEthHeader->ether_type == 1544) {
			itemColor();
			float arpOpcode = pEthArp->ea_hdr.ar_op;
			arpOpcode = arpOpcode / 256.00;
			if (arpOpcode == 5)
				item->setText("DRARP");
			else if (arpOpcode == 8)
				item->setText("InARP");
			else if (arpOpcode == 11)
				item->setText("MARS");
			else
				item->setText("ARP");
		}
		else if (pEthHeader->ether_type == 2048) {
			itemColor();
			item->setText("IP");
		}
		else if (pEthHeader->ether_type == 32821) {
			itemColor();
			item->setText("REVARP");
		}
		else if (pEthHeader->ether_type == 32923) {
			itemColor();
			item->setText("AT");
		}
		else if (pEthHeader->ether_type == 33011) {
			itemColor();
			item->setText("AARP");
		}
		else if (pEthHeader->ether_type == 33024) {
			itemColor();
			item->setText("VLAN");
		}
		else if (pEthHeader->ether_type == 33079) {
			itemColor();
			item->setText("IPX");
		}
		else if (pEthHeader->ether_type == 56710) {
			itemColor();
			item->setText("IPv6");
		}
		else if (pEthHeader->ether_type == 36864) {
			itemColor();
			item->setText("LOOPBACK");
		}
		else {
			itemColor();
			item->setText(QString("0x%1").arg(pEthHeader->ether_type, 4, 16, QLatin1Char('0')));
		}
		table->setItem(rowCount, 3, item);
	}
	{
		item = new QTableWidgetItem;
		itemColor();
		int length = pktHdr.len;
		item->setText(QString::number(length));
		table->setItem(rowCount, 4, item);
	}
	{
		item = new QTableWidgetItem;
		itemColor();
		if (pIpPacket->protocol == IPPROTO_TCP) {
			if (pEthHeader->ether_type == ETHERTYPE_ARP)
				item->setText(QString(" Who has %1 ?").arg(QString::fromLocal8Bit(inet_ntoa(pIpPacket->reqSrcIp.imr_multiaddr))));
			else
				item->setText(QString(" [%1 → %2] Seq: %3  Ack: %4  Len: %5 ").arg(QString::number(ntohs(pTcpPacket->srcPort))).arg(QString::number(ntohs(pTcpPacket->dstPort))).arg(QString::number(ntohs(pTcpPacket->seqNum))).arg(QString::number(ntohs(pTcpPacket->ackNum))).arg(QString::number(ntohs(pIpPacket->totalLen))));
		}
		else 
			item->setText(QString(" [%1 → %2] ").arg(QString::number(ntohs(pTcpPacket->srcPort))).arg(QString::number(ntohs(pTcpPacket->dstPort))));
		table->setItem(rowCount, 5, item);
	}
	rowCount++;
	QString filter = ui->filterLine->text();
	if (!filter.isEmpty() || filterCount) {
		filterCount = 1;
		for (int i = 0; i < rowCount; i++) {
			bool match = true;
			for (int j = 0; j < table->columnCount(); j++) {
				item = table->item(i, j);
				if (item->text().contains(filter)) {
					match = false;
					break;
				}
			}
			table->setRowHidden(i, match);
		}
	}
	if (filter.isEmpty()) {
		filterCount = 0;
		return;
	}
}

void NetTraffic::itemColor()
{
	if (pEthHeader->ether_type == 8)
		item->setData(Qt::BackgroundRole, QColor("#99FFFF"));
	else if (pEthHeader->ether_type == 512)
		item->setData(Qt::BackgroundRole, QColor("#FF9999"));
	else if (pEthHeader->ether_type == 1280)
		item->setData(Qt::BackgroundRole, QColor("#FF6699"));
	else if (pEthHeader->ether_type == 1544)
		item->setData(Qt::BackgroundRole, QColor("#FFFFCC"));
	else if (pEthHeader->ether_type == 2048)
		item->setData(Qt::BackgroundRole, QColor("#99FFFF"));
	else if (pEthHeader->ether_type == 32821)
		item->setData(Qt::BackgroundRole, QColor("#FFCC99"));
	else if (pEthHeader->ether_type == 32923)
		item->setData(Qt::BackgroundRole, QColor("#99CC66"));
	else if (pEthHeader->ether_type == 33011)
		item->setData(Qt::BackgroundRole, QColor("#CCFFCC"));
	else if (pEthHeader->ether_type == 33024)
		item->setData(Qt::BackgroundRole, QColor("#CC9966"));
	else if (pEthHeader->ether_type == 33079)
		item->setData(Qt::BackgroundRole, QColor("#00FFFF"));
	else if (pEthHeader->ether_type == 56710)
		item->setData(Qt::BackgroundRole, QColor("#00FFFF"));
	else 
		item->setData(Qt::BackgroundRole, QColor("#FFFFFF"));
}

void NetTraffic::on_stopButton_clicked()
{
	stop = true;
}

void NetTraffic::on_clearButton_clicked()
{
	if(stop == true) {
		table->clear();
		itemStr.clear();
		rowCount=0;
		createTable();
	}
	else
		QMessageBox::information(this, tr("WARNING"), tr("Press The Stop Button!"));
}

void NetTraffic::resizeEvent(QResizeEvent *event)
{
	table->setColumnWidth(0, this->width() / 9);
	table->setColumnWidth(1, this->width() / 9);
	table->setColumnWidth(2, this->width() / 9);
	table->setColumnWidth(3, this->width() / 9);
	table->setColumnWidth(4, this->width() / 9);
	table->setColumnWidth(5, this->width() / 4);
	QWidget::resizeEvent(event);
} 

NetTraffic::~NetTraffic()
{
	delete ui;
}

